/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_READERDEF_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_READERDEF_HPP_INCLUDED

#include "reader.hpp"

#include <limits>
#include <type_traits>

#include <chopsuey/utility.hpp>

#include "ascii.hpp"
#include "status.hpp"

namespace contagiousbt {
namespace bencode {

template <typename Vtor, typename Sb>
reader<Vtor, Sb>::reader():
  state_(state::pre)
   {}

template <typename Vtor, typename Sb>
int
reader<Vtor, Sb>::read(visitor_type &vtor, streambuf_type &sb)
   {
   using char_type = typename streambuf_type::char_type;

   constexpr auto eof = streambuf_type::traits_type::eof();
   std::make_unsigned_t<typename streambuf_type::int_type> d;
   switch (state_)
      {
    case state::pre:
      {
      auto const rc = vtor.pre_visit(sb);
      if (rc != status_ok)
         return rc;
      }

 tag:
      state_ = state::tag;
      CHOPSUEY_FALLTHROUGH;
    case state::tag:
      {
      auto const tag = sb.sbumpc();
      switch (tag)
         {
       case eof:       return vtor.visit_eof(sb);
       case ascii_i:   num_.delim = ascii_e; break;
       case ascii_ell: goto visit_list_begin;
       case ascii_d:   goto visit_dict_begin;
       case ascii_e:   goto visit_end;
       default:
         d = value_of_ascii_digit(tag);
         if (d <= 9)
            {
            num_.delim = ascii_colon;
            num_.negative = false;
            goto check_leading_zero;
            }
         if (tag == ascii_colon)
            return status_empty_string_length;

         tag_ = static_cast<char_type>(tag);
         goto visit_custom_tag;
         }
      }

      state_ = state::sign;
      CHOPSUEY_FALLTHROUGH;
    case state::sign:
      {
      typename streambuf_type::int_type c;

      c = sb.sbumpc();
      if (c == eof)
         return status_underflow;

      num_.negative = (c == ascii_minus);
      if (num_.negative)
         {
         state_ = state::leading_digit;
         CHOPSUEY_FALLTHROUGH;
    case state::leading_digit:
         c = sb.sbumpc();
         if (c == eof)
            return status_underflow;
         }
      d = value_of_ascii_digit(c);
      if (d <= 9)
         {
 check_leading_zero:
         num_.value = -static_cast<int>(d);
         if (d == 0)
            {
            state_ = state::leading_zero;
            CHOPSUEY_FALLTHROUGH;
    case state::leading_zero:
            c = sb.sbumpc();
            if (c == eof)
               return status_underflow;

            if (static_cast<char_type>(c) == num_.delim)
               {
               if ( !num_.negative)
                  {
                  if (num_.delim == ascii_colon)
                     goto visit_string;

                  goto visit_int;
                  }
               return status_minus_zero;
               }
            return
              value_of_ascii_digit(c) <= 9 ?
              status_leading_zero :
              status_invalid_digit;
            }
         }
      else if (c == ascii_e)
         return num_.negative ? status_malformed_int : status_empty_int;
      else
         return status_invalid_digit;
      }

      state_ = state::digits;
      CHOPSUEY_FALLTHROUGH;
    case state::digits:
      for (;;)
         {
         auto const c = sb.sbumpc();
         if (c == eof)
            return status_underflow;

         d = value_of_ascii_digit(c);
         if (d <= 9)
            {
            using limits = std::numeric_limits<decltype (num_.value)>;
            constexpr auto threshold = -(limits::max() / 10);
            if (num_.value > threshold)
               num_.value =
                 static_cast<decltype (num_.value)>
                   (num_.value * 10 - static_cast<int>(d));
            else if (num_.value == threshold
                     && d
                        <= static_cast<decltype (d)>
                             ( !num_.negative ?
                               limits::max() % 10 :
                               -(limits::min() % 10)))
               num_.value =
                 static_cast<decltype (num_.value)>
                   (threshold * 10 - static_cast<int>(d));
            else
               return status_int_overflow;
            }
         else if (static_cast<char_type>(c) == num_.delim)
            {
            if ( !num_.negative)
               num_.value = static_cast<decltype (num_.value)>(-num_.value);
            if (c == ascii_colon)
               goto visit_string;

            break;
            }
         else
            return status_invalid_digit;
         }

 visit_int:
      state_ = state::visit_int;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_int:
      {
      auto const rc = vtor.visit_int(num_.value, sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

 visit_string:
      state_ = state::visit_string;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_string:
      {
      auto const rc = vtor.visit_string(num_.value, sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

 visit_list_begin:
      state_ = state::visit_list_begin;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_list_begin:
      {
      auto const rc = vtor.visit_list_begin(sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

 visit_dict_begin:
      state_ = state::visit_dict_begin;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_dict_begin:
      {
      auto const rc = vtor.visit_dict_begin(sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

 visit_end:
      state_ = state::visit_end;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_end:
      {
      auto const rc = vtor.visit_end(sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

 visit_custom_tag:
      state_ = state::visit_custom_tag;
      CHOPSUEY_FALLTHROUGH;
    case state::visit_custom_tag:
      auto const rc =
        vtor.visit_custom_tag
          (static_cast<typename visitor_type::tag_type>(tag_), sb);
      if (rc == status_ok)
         goto tag;

      return rc;
      }

   CHOPSUEY_NOTREACHED
   }

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_READERDEF_HPP_INCLUDED */
