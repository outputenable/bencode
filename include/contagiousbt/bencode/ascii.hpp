/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016, 2019.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_ASCII_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_ASCII_HPP_INCLUDED

#include <type_traits>

#include <chopsuey/utility.hpp>

namespace contagiousbt {
namespace bencode {

enum ascii: char
   {
   ascii_minus = 0x2d,

   ascii_0 = 0x30,
   ascii_1 = 0x31,
   ascii_2 = 0x32,
   ascii_3 = 0x33,
   ascii_4 = 0x34,
   ascii_5 = 0x35,
   ascii_6 = 0x36,
   ascii_7 = 0x37,
   ascii_8 = 0x38,
   ascii_9 = 0x39,

   ascii_colon = 0x3a,

   ascii_d   = 0x64,
   ascii_e   = 0x65,
   ascii_i   = 0x69,
   ascii_ell = 0x6c
   };

using chopsuey::enums::operator*;

template <typename T>
std::make_unsigned_t<T>
value_of_ascii_digit(T const c) noexcept
   {
   return
     static_cast<decltype (value_of_ascii_digit(c))>
       (chopsuey::to_unsigned(c) - ascii_0);
   }

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_ASCII_HPP_INCLUDED */
