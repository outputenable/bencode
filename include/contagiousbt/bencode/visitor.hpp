/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_VISITOR_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_VISITOR_HPP_INCLUDED

#include <cstdint>

namespace contagiousbt {
namespace bencode {

template <typename Sb>
class visitor
   {
 public:
   using streambuf_type = Sb;
   using tag_type = typename streambuf_type::char_type;
   using int_type = std::int_least64_t;

   virtual ~visitor() = default;

   virtual int pre_visit(streambuf_type &sb);

   virtual int visit_int(int_type value, streambuf_type &sb) = 0;

   virtual int visit_string(int_type &length, streambuf_type &sb) = 0;

   virtual int visit_list_begin(streambuf_type &sb) = 0;

   virtual int visit_dict_begin(streambuf_type &sb) = 0;

   virtual int visit_end(streambuf_type &sb) = 0;

   virtual int visit_custom_tag(tag_type tag, streambuf_type &sb);

   virtual int visit_eof(streambuf_type &sb);

 protected:
   visitor &operator=(visitor const &) = default;
   visitor &operator=(visitor &&) = default;
   };

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_VISITOR_HPP_INCLUDED */
