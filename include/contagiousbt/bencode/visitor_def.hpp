/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_VISITORDEF_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_VISITORDEF_HPP_INCLUDED

#include "visitor.hpp"

#include "status.hpp"

namespace contagiousbt {
namespace bencode {

template <typename Sb>
int
visitor<Sb>::pre_visit(streambuf_type &)
   {
   return status_ok;
   }

template <typename Sb>
int
visitor<Sb>::visit_custom_tag(tag_type, streambuf_type &)
   {
   return status_invalid_tag;
   }

template <typename Sb>
int
visitor<Sb>::visit_eof(streambuf_type &)
   {
   return status_underflow;
   }

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_VISITORDEF_HPP_INCLUDED */
