/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_READER_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_READER_HPP_INCLUDED

#include <type_traits>

namespace contagiousbt {
namespace bencode {

template <typename Vtor, typename Sb>
class reader
   {
 public:
   static_assert
     (std::is_constructible
        <typename Vtor::tag_type, typename Sb::char_type>::value,
      "Vtor::tag_type is supposed to be constructible from Sb::char_type");

   static_assert
     ((   std::is_signed<typename Vtor::int_type>::value
       && std::is_integral<typename Vtor::int_type>::value),
      "Vtor::int_type is supposed to be a signed integer type");

   using visitor_type = Vtor;
   using streambuf_type = Sb;

   using int_type = typename visitor_type::int_type;

   reader();

   reader(reader const &) = delete;
   reader(reader &&) = default;

   reader &operator=(reader const &) = delete;
   reader &operator=(reader &&) = default;

   int read(visitor_type &vtor, streambuf_type &sb);

 private:
   // TODO - According to C++14, 14.5.1.4 Enumeration members of class
   //        templates [temp.mem.enum] it should be possible to just forward
   //        declare state here and move the definition into reader_def.hpp,
   //        but VS 2015 Update 3 complains with error C3113.
   enum class state
      {
      pre,
      tag,
      sign,
      leading_digit,
      leading_zero,
      digits,
      visit_int,
      visit_string,
      visit_list_begin,
      visit_dict_begin,
      visit_end,
      visit_custom_tag
      };

   state state_;

   struct number
      {
      int_type value;
      typename streambuf_type::char_type delim;
      bool negative;
      };

   union
      {
      number num_;
      typename streambuf_type::char_type tag_;
      };
   };

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_READER_HPP_INCLUDED */
