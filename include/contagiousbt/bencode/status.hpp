/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_STATUS_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_STATUS_HPP_INCLUDED

namespace contagiousbt {
namespace bencode {

enum
   {
   status_key_missing_value        = 7,
   status_keys_not_in_sorted_order = 6,
   status_non_string_key           = 5,
   status_stray_end_tag            = 4,
   status_invalid_tag              = 3,
   status_overflow                 = 2,
   status_done                     = 1,

   status_ok = 0,

   status_bad_stream          = -1,
   status_underflow           = -2,
   status_empty_string_length = -3,
   status_empty_int           = -4,
   status_invalid_digit       = -5,
   status_leading_zero        = -6,
   status_malformed_int       = -7,
   status_minus_zero          = -8,
   status_int_overflow        = -9
   };

inline char const *
strstatus(int const status) noexcept
   {
   switch (status)
      {
    case status_key_missing_value: return "Dictionary key missing value";
    case status_keys_not_in_sorted_order: return "Dictionary keys not in sorted order";
    case status_non_string_key:    return "Dictionary key not of type string";
    case status_stray_end_tag:     return "Stray end tag";
    case status_invalid_tag:       return "Invalid tag";
    case status_overflow:          return "Overflow";
    case status_done:              return "Done";

    case status_ok: return "No error";

    case status_bad_stream:          return "Bad stream";
    case status_underflow:           return "Stream buffer underflow";
    case status_empty_string_length: return "Empty string length field";
    case status_empty_int:           return "Empty integer item";
    case status_invalid_digit:       return "Invalid digit";
    case status_leading_zero:        return "Number with leading zeros";
    case status_malformed_int:       return "Malformed integer";
    case status_minus_zero:          return "Integer value minus zero";
    case status_int_overflow:        return "Integer overflow";

    default: return "n/a";
      }
   }

} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_STATUS_HPP_INCLUDED */
