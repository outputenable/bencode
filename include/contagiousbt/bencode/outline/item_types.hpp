/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_ITEMTYPE_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_ITEMTYPE_HPP_INCLUDED

#include <cstdint>

#include <boost/optional/optional.hpp>
#include <boost/utility/string_view.hpp>

namespace contagiousbt {
namespace bencode {
namespace outline {

enum class item_type: char
   {
   list    = 0b000,
   end     = 0b001,
   string  = 0b011,
   dict    = 0b110,
   integer = 0b111
   };

using integer = std::int_least64_t;

template <typename T> using basic_string = boost::basic_string_view<T>;

using string = basic_string<char>;

template <typename T> using optional = boost::optional<T>;

constexpr bool is_container(item_type const t) noexcept
   {
   return (static_cast<int>(t) & 1) == 0;
   }

} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_ITEMTYPE_HPP_INCLUDED */
