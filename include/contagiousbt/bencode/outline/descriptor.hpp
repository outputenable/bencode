/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016, 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTOR_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTOR_HPP_INCLUDED

// This file contains the outline descriptor proper.  It completes the
// descriptor base facilities by adding support for list and dictionary
// container items in the form of descriptor *ranges*.

#include <tuple>
#include <utility>

#include <boost/iterator/iterator_facade.hpp>

#include "descriptor_base.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {

// The Bencode format encodes a hierarchical structure into a flat sequence of
// tokens/items in a depth-first approach.  Consequently, descriptor ranges
// come in two basic flavours regarding their iteration behaviour.  "Flat"
// ranges use a *physical* stride and iterate over the items in the sequence
// they appear in the bencoded source data (possibly including end tag or 'e'
// items!).  Iteration thus proceeds across hierarchical levels, also in a
// depth-first manner and with no regard to list or dictionary boundaries.
// This is probably most useful for source-to-source transformations that
// preserve the sequence of items, e.g. Bencode-to-XML or the like.  Otherwise
// ranges with a *logical* stride come handy.  These obey the hierarchical
// structure and iterate over just the sequence of items at a single level of
// the hierarchy in a breadth-first manner, e.g. the immediate items of a list
// or dictionary (excluding the end tag or 'e' item!).  Each iteration step
// may thus skip several (nested) items in the physical sequence that are
// logically located at lower levels in the hierarchy.

enum class stride { physical, logical };

template <stride, typename> class range_base;
template <typename> class flat_range;
template <typename> class range;
template <typename> class key_value_range;

template <typename charT>
class basic_descriptor: public basic_descriptor_base<charT>
   {
   friend class range_base<stride::physical, basic_descriptor>;
   friend class range_base<stride::logical, basic_descriptor>;
   friend class key_value_range<basic_descriptor>;

 public:
   basic_descriptor() noexcept;

   basic_descriptor(detail::cell const *const cs, void const *const be):
     basic_descriptor_base<charT>(cs, be)
      {}

   optional<range<basic_descriptor>> get_range() const;

   range<basic_descriptor> to_range() const;

   optional<key_value_range<basic_descriptor>> get_kv_range() const;

   key_value_range<basic_descriptor> to_kv_range() const;

   optional<flat_range<basic_descriptor>> get_flat_range() const;

   flat_range<basic_descriptor> to_flat_range() const;
   };

template <typename charT>
basic_descriptor<charT>::basic_descriptor() noexcept = default;

// this draws inspiration from the ideas presented in Eric Niebler's blog post
// "Input Iterators vs Input Ranges" as well as in the comments there:
// http://ericniebler.com/2013/11/07/input-iterators-vs-input-ranges/

template <typename D, typename V>
class range_iterators
   {
 public:
   using const_iterator = class iterator:
     public boost::iterator_facade
       <iterator, V const, boost::single_pass_traversal_tag>
      {
      D *rng_;

    public:
      iterator() noexcept: rng_() {}

    private:
      friend class boost::iterator_core_access;
      friend class range_iterators;

      explicit iterator(D &rng): rng_(rng ? &rng : nullptr) {}

      bool equal(iterator const rhs) const { return rng_ == rhs.rng_; }

      decltype (auto) dereference() const { return rng_->get(); }

      void increment()
         {
         rng_->next();
         if ( !*rng_)
            rng_ = nullptr;
         }
      };

   const_iterator cbegin() { return const_iterator(static_cast<D &>(*this)); }

   const_iterator cend() { return const_iterator(); }

   iterator begin() { return iterator(static_cast<D &>(*this)); }

   iterator end() { return iterator(); }

 protected:
   range_iterators() = default;

   range_iterators(range_iterators const &) = default;
   range_iterators(range_iterators &&) = default;

   ~range_iterators() = default;

   range_iterators &operator=(range_iterators const &) = default;
   range_iterators &operator=(range_iterators &&) = default;
   };

template <stride Stride, typename Descriptor>
class range_base:
  public range_iterators<range_base<Stride, Descriptor>, Descriptor>
   {
 public:
   using value_type = Descriptor;

 private:
   value_type value_;
   detail::cell const *last_;

 public:
   explicit operator bool() const { return !this->done(); }

   bool done() const { return this->first() == this->last(); }

   value_type const &get() const { return value_; }

   value_type const &operator*() const { return this->get(); }

   value_type const *operator->() const { return &this->get(); }

   void next()
      {
      value_.cells +=
        Stride == stride::physical ?
        value_.num_cells() :
        value_.cells_total();
      }

 protected:
   range_base() noexcept: last_() {}

   range_base
     (detail::cell const *const first, detail::cell const *const last,
      void const *const be):
     value_(first, be),
     last_(last)
      {}

   range_base(range_base const &) = default;
   range_base(range_base &&) = default;

   ~range_base() = default;

   range_base &operator=(range_base const &) = default;
   range_base &operator=(range_base &&) = default;

   detail::cell const *first() const { return value_.cells; }

   detail::cell const *last() const { return last_; }

   void const *be_base() const { return value_.be_base; }
   };

template <typename Descriptor>
class flat_range: public range_base<stride::physical, Descriptor>
   {
 public:
   using descriptor_type = Descriptor;

   flat_range() noexcept;

   flat_range
     (detail::cell const *const first, detail::cell const *const last,
      void const *const be):
     range_base<stride::physical, Descriptor>(first, last, be)
      {}

   flat_range &operator++()
      {
      this->next();
      return *this;
      }

   flat_range operator++(int)
      {
      auto tmp(*this);
      this->operator++();
      return tmp;
      }
   };

template <typename Descriptor>
flat_range<Descriptor>::flat_range() noexcept = default;

template <typename Descriptor>
class range: public range_base<stride::logical, Descriptor>
   {
 public:
   using descriptor_type = Descriptor;

   range() noexcept;

   range
     (detail::cell const *const first, detail::cell const *const last,
      void const *const be):
     range_base<stride::logical, Descriptor>(first, last, be)
      {}

   flat_range<descriptor_type> to_flat_range() const
      {
      return { this->first(), this->last(), this->be_base() };
      }

   range &operator++()
      {
      this->next();
      return *this;
      }

   range operator++(int)
      {
      auto tmp(*this);
      this->operator++();
      return tmp;
      }
   };

template <typename charT>
range<charT>::range() noexcept = default;

template <typename charT>
optional<range<basic_descriptor<charT>>>
basic_descriptor<charT>::get_range() const
   {
   if (this->is_container())
      return this->to_range();

   return {};
   }

template <typename charT>
range<basic_descriptor<charT>>
basic_descriptor<charT>::to_range() const
   {
   auto const first = this->cells + this->num_cells();
   return { first, first + this->nested_cells(), this->be_base };
   }

template <typename charT>
optional<flat_range<basic_descriptor<charT>>>
basic_descriptor<charT>::get_flat_range() const
   {
   if (this->is_container())
      return this->to_flat_range();

   return {};
   }

template <typename charT>
flat_range<basic_descriptor<charT>>
basic_descriptor<charT>::to_flat_range() const
   {
   auto const first = this->cells + this->num_cells();
   return { first, first + this->nested_cells(), this->be_base };
   }

template <typename Descriptor>
class key_value_range:
  public range_iterators
    <key_value_range<Descriptor>, std::pair<Descriptor, Descriptor>>
   {
 public:
   using descriptor_type = Descriptor;

   using value_type = std::pair<descriptor_type, descriptor_type>;

 private:
   value_type value_;
   detail::cell const *last_;

 public:
   key_value_range() noexcept : last_() {}

   key_value_range
     (detail::cell const *const first, detail::cell const *const last,
      void const *const be):
     value_
       (std::piecewise_construct,
        std::forward_as_tuple(first, be),
        std::forward_as_tuple(first + detail::cells_total(first), be)),
     last_(last)
      {}

   explicit operator bool() const { return !this->done(); }

   bool done() const { return this->first() == this->last(); }

   value_type const &get() const { return value_; }

   value_type const &operator*() const { return this->get(); }

   value_type const *operator->() const { return &this->get(); }

   void next()
      {
      value_.first.cells = value_.second.cells + value_.second.cells_total();
      value_.second.cells = value_.first.cells + value_.first.cells_total();
      }

   key_value_range &operator++()
      {
      this->next();
      return *this;
      }

   key_value_range operator++(int)
      {
      auto tmp(*this);
      this->operator++();
      return tmp;
      }

   bool find(typename descriptor_type::string_type const key)
      {
      while ( !this->done())
         {
         auto const &k = this->get().first;
         if (k.is_string() && k.to_string() == key)
            return true;

         this->next();
         }
      return false;
      }

   range<descriptor_type> to_range() const
      {
      return { this->first(), this->last(), this->be_base() };
      }

   flat_range<descriptor_type> to_flat_range() const
      {
      return { this->first(), this->last(), this->be_base() };
      }

 protected:
   detail::cell const *first() const { return value_.first.cells; }

   detail::cell const *last() const { return last_; }

   void const *be_base() const { return value_.first.be_base; }
   };

template <typename charT>
optional<key_value_range<basic_descriptor<charT>>>
basic_descriptor<charT>::get_kv_range() const
   {
   if (this->is_dict())
      return this->to_kv_range();

   return {};
   }

template <typename charT>
key_value_range<basic_descriptor<charT>>
basic_descriptor<charT>::to_kv_range() const
   {
   auto const first = this->cells + this->num_cells();
   return { first, first + this->nested_cells(), this->be_base };
   }

using descriptor = basic_descriptor<char>;

} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTOR_HPP_INCLUDED */
