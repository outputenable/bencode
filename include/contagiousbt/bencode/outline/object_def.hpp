/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECTDEF_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECTDEF_HPP_INCLUDED

#include "object.hpp"

#include <stack>

#if defined _MSC_VER
#  pragma warning( push )
#  pragma warning( disable : 4365 )
#endif
#include <boost/container/small_vector.hpp>
#if defined _MSC_VER
#  pragma warning( pop )
#endif

#include <contagiousbt/bencode/reader_def.hpp>

#include "detail/membuf.hpp"
#include "detail/writer_def.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {

template <typename charT, typename Allocator>
typename gsl::span<typename object<charT, Allocator>::char_type const>
  ::index_type
object<charT, Allocator>::outline
  (gsl::span<char_type const> const buf,
   std::size_t const max_top_level_items,
   int &ec)
   {
   this->clear();
   be_ = buf.data();

   using stack_type =
     std::stack
       <detail::stack_frame,
        boost::container::small_vector
          <detail::stack_frame, 10,
           typename std::allocator_traits<allocator_type>
             ::template rebind_alloc<detail::stack_frame>>>;
   stack_type stk
     (typename stack_type::container_type::allocator_type
        (cells_.get_allocator()));

   detail::basic_membuf<char_type> mb(buf);
   detail::writer<decltype (cells_), decltype (stk), decltype (mb)> wr
     (cells_, stk, max_top_level_items);
   reader<decltype (wr), decltype (mb)> rd;

   ec = rd.read(wr, mb);

   auto const off = get_stream_pos(mb);
   if (ec == status_ok || ec == status_done)
      {
      if (off <= detail::max_offset)
         // String descriptors assume that there is always a next item cell in
         // order to calculate the string length.  This will not be part of
         // the object's range though.
         cells_.push_back
           (detail::cell((off << detail::tag_bits) | detail::end_tag));
      else
         ec = status_overflow;
      }

   return static_cast<typename decltype (buf)::index_type>(off);
   }

template <typename charT, typename Allocator>
optional<descriptor>
object<charT, Allocator>::root_dict() const
   {
   if (auto rr = this->root_range())
      if (rr->is_dict())
         {
         auto dd = *rr;
         if ( !++rr)
            return dd;
         }
   return {};
   }

} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECTDEF_HPP_INCLUDED */
