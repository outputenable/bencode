/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016, 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTORBASE_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTORBASE_HPP_INCLUDED

// This file defines the descriptor base class which provides facilities that
// are common to all item types, as well as specific operations for the
// non-container types integer and string.  The implementation of list and
// dictionary container-specific functionality is left to the actual (derived)
// descriptor type.

#include <cstddef>
#include <cstdint>
#include <limits>
#include <type_traits>

#include <gsl/span>

#include "detail/cell.hpp"
#include "detail/decode.hpp"
#include "item_types.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {

template <typename charT>
class basic_descriptor_base
   {
 public:
   using char_type = charT;

   using int_type = integer;

   using string_type = basic_string<char_type>;

 protected:
   detail::cell const *cells;

   char_type const *be_base;

 public:
   explicit operator bool() const { return this->cells != nullptr; }

   item_type type() const { return type_of(this->cells[0]); }

   bool is_int() const { return this->type() == item_type::integer; }

   bool is_container() const { return detail::is_container(this->cells[0]); }

   bool is_string() const
      {
      return get_tag(this->cells[0]) == detail::string_tag;
      }

   bool is_list() const
      {
      return get_tag(this->cells[0]) == detail::list_tag;
      }

   bool is_dict() const
      {
      return get_tag(this->cells[0]) == detail::dict_tag;
      }

   optional<int_type> get_int() const
      {
      if (this->is_int())
         return this->to_int();

      return {};
      }

   int_type to_int() const { return get_integer(this->cells); }

   optional<string_type> get_string() const
      {
      if (this->is_string())
         return this->to_string();

      return {};
      }

   string_type to_string() const
      {
      return
        { (reinterpret_cast<char_type const *>(this->be_base)
           + get_string_offset(this->cells)),
          get_string_length(this->cells) };
      }

   std::size_t size() const
      {
      return static_cast<std::size_t>(this->cells[2]);
      }

   std::ptrdiff_t be_offset() const { return get_offset(this->cells[0]); }

   char_type const *be_data() const
      {
      return this->be_base + this->be_offset();
      }

   gsl::span<char_type const> be_view() const;

 protected:
   basic_descriptor_base() noexcept: cells() {}

   basic_descriptor_base(detail::cell const *const cs, void const *const be):
     cells(cs),
     be_base(static_cast<char_type const *>(be))
      {}

   basic_descriptor_base(basic_descriptor_base const &) = default;
   basic_descriptor_base(basic_descriptor_base &&) = default;

   ~basic_descriptor_base() = default;

   basic_descriptor_base &operator=(basic_descriptor_base const &) = default;
   basic_descriptor_base &operator=(basic_descriptor_base &&) = default;

   std::ptrdiff_t num_cells() const
      {
      return detail::num_cells(this->cells[0]);
      }

   std::ptrdiff_t nested_cells() const
      {
      return detail::nested_cells(this->cells);
      }

   std::ptrdiff_t cells_total() const
      {
      return detail::cells_total(this->cells);
      }
   };

template <typename charT>
gsl::span<typename basic_descriptor_base<charT>::char_type const>
basic_descriptor_base<charT>::be_view() const
   {
   auto const last = this->cells + this->cells_total();
   auto const offset = this->be_offset();
   auto const count = static_cast<std::ptrdiff_t>(get_offset(*last) - offset);
   auto const s = this->be_base + offset;
   return { s, count };
   }

using descriptor_base = basic_descriptor_base<char>;

} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DESCRIPTORBASE_HPP_INCLUDED */
