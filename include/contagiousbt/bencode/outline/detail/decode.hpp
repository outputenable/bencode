/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016, 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_DECODE_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_DECODE_HPP_INCLUDED

// This file contains various functions for extracting and decoding
// information from a group of cells.

#include <cstddef>

#include <chopsuey/utility.hpp>

#include <contagiousbt/bencode/outline/item_types.hpp>

#include "cell.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {
namespace detail {

constexpr tag get_tag(cell const &c) noexcept
   {
   return static_cast<tag>(c & tag_mask);
   }

constexpr std::ptrdiff_t get_offset(cell const &c) noexcept
   {
   return static_cast<std::ptrdiff_t>(c >> tag_bits);
   }

constexpr item_type to_item_type(tag const t) noexcept
   {
   return static_cast<item_type>(t | (t >> 1));
   }

constexpr item_type type_of(cell const &c) noexcept
   {
   return to_item_type(get_tag(c));
   }

constexpr bool is_container(cell const &c) noexcept
   {
   return (c & 1) == 0;
   }

constexpr std::ptrdiff_t num_cells(tag const t) noexcept
   {
   return static_cast<std::ptrdiff_t>((0b101110011U >> t) & 0b11U);
   }

constexpr std::ptrdiff_t num_cells(cell const &c) noexcept
   {
   return num_cells(get_tag(c));
   }

inline std::ptrdiff_t nested_cells(cell const *const cs)
   {
   return static_cast<std::ptrdiff_t>(cs[1] - num_cells(end_tag));
   }

inline std::ptrdiff_t cells_total(cell const *const cs)
   {
   auto const m = static_cast<cell::value_type>( !is_container(cs[0])) - 1U;
   auto n = static_cast<std::ptrdiff_t>(cs[1] & m);
   n += num_cells(*cs);
   return n;
   }

inline integer
get_integer(cell const *const cs)
   {
   if (get_tag(cs[0]) == int2_tag)
      return chopsuey::as_signed(cs[1].value);

   auto const u =
     static_cast<std::make_unsigned_t<integer>>
       ((static_cast<std::make_unsigned_t<integer>>(cs[1])
         << std::numeric_limits<cell::value_type>::digits)
        | cs[2]);
   return chopsuey::as_signed(u);
   }

inline std::ptrdiff_t
get_string_offset(cell const *const cs)
   {
   return get_offset(cs[0]) + static_cast<std::ptrdiff_t>(cs[1]);
   }

inline std::size_t
get_string_length(cell const *const cs)
   {
   auto const next = cs + num_cells(*cs);
   return static_cast<std::size_t>(get_offset(*next) - get_string_offset(cs));
   }

} // namespace detail
} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_DECODE_HPP_INCLUDED */
