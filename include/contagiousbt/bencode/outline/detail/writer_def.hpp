/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITERDEF_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITERDEF_HPP_INCLUDED

#include "writer.hpp"

#include <algorithm>
#include <ios>
#include <limits>

#include <chopsuey/utility.hpp>

#include <contagiousbt/bencode/status.hpp>
#include <contagiousbt/bencode/outline/descriptor.hpp>

#include "decode.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {
namespace detail {

template <typename T> auto get_stream_pos(T &sb)
   {
   return sb.pubseekoff(0, std::ios_base::cur, std::ios_base::in);
   }

template <typename T> auto invalid_pos(T const &)
   {
   return typename T::pos_type(typename T::off_type(-1));
   }

template <typename C, typename Stk, typename Sb>
std::make_unsigned_t<typename writer<C, Stk, Sb>::streambuf_type::off_type>
writer<C, Stk, Sb>::get_offset() const
   {
   return static_cast<decltype (this->get_offset())>(last_pos_ - init_pos_);
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::pre_visit(streambuf_type &sb)
   {
   if ( !top_level_items_remain_)
      return status_done;

   last_pos_ = get_stream_pos(sb);
   if (last_pos_ == invalid_pos(sb))
      return status_bad_stream;

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_int(int_type const value, streambuf_type &sb)
   {
   auto const top = !stack_->empty() ? &stack_->top() : nullptr;
   if (top && top->expects_key())
      return status_non_string_key;

   auto const offset = this->get_offset();
   if (offset > max_offset)
      return status_overflow;

   constexpr auto int2_max = std::numeric_limits<cell::svalue_type>::max();
   constexpr auto int2_min = std::numeric_limits<cell::svalue_type>::min();
   auto const tag =
     value <= int2_max && value >= int2_min ? int2_tag : int3_tag;

   cells_->push_back(cell((offset << tag_bits) | tag));

   if (tag == int2_tag)
      {
      auto const i = static_cast<cell::svalue_type>(value);
      cells_->push_back(cell(chopsuey::as_unsigned(i)));
      }
   else
      {
      // Must be able to store the integer representation in two cells.
      static_assert(std::is_unsigned<cell::value_type>::value, "");
      static_assert
        (std::numeric_limits<std::make_unsigned_t<int_type>>::digits
         <= 2 * std::numeric_limits<cell::value_type>::digits, "");

      auto const &u = chopsuey::as_unsigned(value);
      cells_->push_back
        (cell(u >> std::numeric_limits<cell::value_type>::digits));
      cells_->push_back
        (cell(u & std::numeric_limits<cell::value_type>::max()));
      }

   last_pos_ = get_stream_pos(sb);

   if (top)
      ++top->items;
   else if ( !--top_level_items_remain_)
      return status_done;

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_string(int_type &length, streambuf_type &sb)
   {
   using uint_type = std::make_unsigned_t<int_type>;
   if (static_cast<uint_type>(length)
       > std::min
           ({ static_cast<uint_type>
                (std::numeric_limits<cell::value_type>::max()),
              static_cast<uint_type>
                (std::numeric_limits<std::size_t>::max()),
              static_cast<uint_type>
                (std::numeric_limits<typename streambuf_type::off_type>
                   ::max()) }))
      return status_overflow;

   auto const offset = this->get_offset();
   if (offset > max_offset)
      return status_overflow;

   auto expects_key = false;

   // the stream is positioned at the first char of the string
   auto const string_pos = get_stream_pos(sb);

   if ( !stack_->empty())
      {
      auto &top = stack_->top();
      expects_key = top.expects_key();
      if (expects_key && top.items != 0)
         {
         auto const last_key_idx =
           static_cast<typename cells_type::size_type>(top.last_key_index);
         auto const be =
           sb.data()
           + static_cast<std::ptrdiff_t>
               (static_cast<typename streambuf_type::off_type>(init_pos_));
         auto const last_key =
           descriptor(&(*cells_)[last_key_idx], be).to_string();
         string const key
           (be + static_cast<typename streambuf_type::off_type>(string_pos),
            static_cast<std::size_t>(length));
         if ( !(last_key < key))
            return status_keys_not_in_sorted_order;
         }
      }

   // skip over length chars
   auto const pos =
     sb.pubseekoff
       (static_cast<typename streambuf_type::off_type>(length),
        std::ios_base::cur, std::ios_base::in);
   if (pos == invalid_pos(sb)
       || static_cast<typename streambuf_type::off_type>(pos - string_pos)
          != length)
      return status_bad_stream;

   cells_->push_back(cell((offset << tag_bits) | string_tag));
   cells_->push_back(cell(string_pos - last_pos_));

   last_pos_ = pos;

   if ( !stack_->empty())
      {
      auto &top = stack_->top();
      ++top.items;
      if (expects_key)
         top.last_key_index =
           static_cast<decltype (top.last_key_index)>
             (cells_->size()
              - static_cast<std::size_t>(num_cells(string_tag)));
      }
   else if ( !--top_level_items_remain_)
      return status_done;

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_list_begin(streambuf_type &sb)
   {
   if ( !stack_->empty() && stack_->top().expects_key())
      return status_non_string_key;

   auto const offset = this->get_offset();
   if (offset > max_offset)
      return status_overflow;

   stack_->emplace
     (stack_frame::list, static_cast<cell::value_type>(cells_->size()));

   cells_->push_back(cell((offset << tag_bits) | list_tag));
   // to be filled in by visit_end():
   cells_->emplace_back();  // #cells
   cells_->emplace_back();  // #elems

   last_pos_ = get_stream_pos(sb);

   // item will be added to a possible enclosing container in visit_end()

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_dict_begin(streambuf_type &sb)
   {
   if ( !stack_->empty() && stack_->top().expects_key())
      return status_non_string_key;

   auto const offset = this->get_offset();
   if (offset > max_offset)
      return status_overflow;

   stack_->emplace
     (stack_frame::dict, static_cast<cell::value_type>(cells_->size()));

   cells_->push_back(cell((offset << tag_bits) | dict_tag));
   // to be filled in by visit_end():
   cells_->emplace_back();  // #cells
   cells_->emplace_back();  // #elems (key/value pairs)

   last_pos_ = get_stream_pos(sb);

   // item will be added to a possible enclosing container in visit_end()

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_end(streambuf_type &sb)
   {
   if (stack_->empty())
      return status_stray_end_tag;

   auto const offset = this->get_offset();
   if (offset > max_offset)
      return status_overflow;

   auto &top = stack_->top();

   auto nelems = top.items;
   if (top.is_dict())
      {
      if (nelems % 2)
         return status_key_missing_value;

      nelems /= 2;  // #key/value pairs
      }

   cells_->push_back(cell((offset << tag_bits) | end_tag));

   auto const c =
     &(*cells_)[static_cast<typename cells_type::size_type>(top.index())];

   // ncells includes the end tag but not the initial num_cells(container_tag)
   static_assert(num_cells(dict_tag) == num_cells(list_tag), "");
   auto const ncells = &cells_->back() + 1 - c - num_cells(dict_tag);

   c[1] = ncells;
   c[2] = nelems;

   last_pos_ = get_stream_pos(sb);

   stack_->pop();

   // naturally, this container item may itself be nested inside another
   // container
   if ( !stack_->empty())
      ++stack_->top().items;
   else if ( !--top_level_items_remain_)
      return status_done;

   return status_ok;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_custom_tag(tag_type, streambuf_type &)
   {
   return status_invalid_tag;
   }

template <typename C, typename Stk, typename Sb>
int
writer<C, Stk, Sb>::visit_eof(streambuf_type &)
   {
   return stack_->empty() ? status_done : status_underflow;
   }

} // namespace detail
} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITERDEF_HPP_INCLUDED */
