/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITER_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITER_HPP_INCLUDED

#include <cstddef>
#include <cstdint>
#include <limits>
#include <type_traits>

#include <contagiousbt/bencode/outline/item_types.hpp>

#include "cell.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {
namespace detail {

class stack_frame
   {
 public:
   cell::value_type items;
   cell::value_type last_key_index;

 private:
   // XXX One input char may generate at most three cells, and by
   //     providing a sufficient number of bits we can avoid having to
   //     check the index since we're already checking the offset.
   cell::value_type index_and_is_dict_;
   static_assert
     (std::numeric_limits<decltype (index_and_is_dict_)>::digits - 1
      >= offset_bits + 2, "");

 public:
   enum container_type { list = 0, dict = 1 };

   stack_frame() noexcept = default;

   stack_frame(container_type const typ, cell::value_type const idx) noexcept:
     items(0),
     index_and_is_dict_
       (static_cast<cell::value_type>
          ((idx << 1) | static_cast<unsigned int>(typ)))
      {}

   bool is_dict() const noexcept
      {
      return (this->index_and_is_dict_ & dict) != 0;
      }

   cell::value_type index() const noexcept
      {
      return static_cast<cell::value_type>(this->index_and_is_dict_ >> 1);
      }

   bool expects_key() const noexcept
      {
      return
        (static_cast<unsigned int>(this->is_dict()) & (this->items ^ 1)) != 0;
      }
   };

template <typename C, typename Stk, typename Sb>
class writer
   {
 public:
   using cells_type = C;

   static_assert
     (std::is_same<typename Stk::value_type, stack_frame>::value, "");
   using stack_type = Stk;

   using streambuf_type = Sb;
   using int_type = integer;
   using tag_type = typename streambuf_type::char_type;

 private:
   using pos_type = typename streambuf_type::pos_type;

   pos_type last_pos_;
   pos_type init_pos_;
   cells_type *cells_;
   stack_type *stack_;
   std::size_t top_level_items_remain_;

 public:
   writer
     (cells_type &cells,
      stack_type &stack,
      std::size_t const max_top_level_items =
        std::numeric_limits<std::size_t>::max()):
     // last_pos_ is set in pre_visit()
     init_pos_(0),
     cells_(&cells),
     stack_(&stack),
     top_level_items_remain_(max_top_level_items)
      {}

   int pre_visit(streambuf_type &sb);

   int visit_int(int_type value, streambuf_type &sb);

   int visit_string(int_type &length, streambuf_type &sb);

   int visit_list_begin(streambuf_type &sb);

   int visit_dict_begin(streambuf_type &sb);

   int visit_end(streambuf_type &sb);

   int visit_custom_tag(tag_type tag, streambuf_type &sb);

   int visit_eof(streambuf_type &sb);

 private:
   std::make_unsigned_t<typename streambuf_type::off_type> get_offset() const;
   };

} // namespace detail
} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_WRITER_HPP_INCLUDED */
