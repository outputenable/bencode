/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_MEMBUF_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_MEMBUF_HPP_INCLUDED

#include <cstddef>
#include <ios>
#include <string>

#include <gsl/span>

namespace contagiousbt {
namespace bencode {
namespace outline {
namespace detail {

template <typename charT, typename traits = std::char_traits<charT>>
class basic_membuf
   {
 public:
   using char_type = charT;
   using int_type = typename traits::int_type;
   using pos_type = std::size_t;
   using off_type = std::ptrdiff_t;
   using traits_type = traits;

 private:
   char_type const *pos_;
   char_type const *end_;
   char_type const *org_;

 public:
   basic_membuf(gsl::span<char_type const> const in):
     pos_(in.data()),
     end_(pos_ + in.size()),
     org_(pos_)
      {}

   char_type const *data() const { return org_; }

   int_type sbumpc()
      {
      return
        pos_ != end_ ? traits_type::to_int_type(*pos_++) : traits_type::eof();
      }

   pos_type pubseekoff
     (off_type const off,
      std::ios_base::seekdir /*way*/,
      std::ios_base::openmode /*which*/ =
        std::ios_base::in | std::ios_base::out)
      {
      if (end_ - pos_ >= off)
         {
         pos_ += off;
         return static_cast<pos_type>(pos_ - org_);
         }
      return pos_type(off_type(-1));
      }
   };

using membuf = basic_membuf<char>;

} // namespace detail
} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_MEMBUF_HPP_INCLUDED */
