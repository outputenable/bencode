/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_CELL_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_CELL_HPP_INCLUDED

#include <cstdint>
#include <limits>
#include <type_traits>

#include <gsl/gsl_util>

#include <chopsuey/type_traits.hpp>

namespace contagiousbt {
namespace bencode {
namespace outline {
namespace detail {

// The cell is basically a "strong typedef" for an unsigned 32-bit
// integer.  Each outlined Bencoded element is represented by one or
// more cells, and the core of an outline is just a (possibly very
// long) sequence of such groups of cells.
struct cell
   {
   using value_type = std::uint_least32_t;
   using svalue_type = std::make_signed_t<value_type>;

   value_type value;

   cell() noexcept = default;

   template <typename T>
   explicit constexpr cell
     (T const arg,
      std::enable_if_t<chopsuey::is_integer<T>::value> * = nullptr) noexcept:
     value(gsl::narrow_cast<value_type>(arg))
      {}

   template <typename T>
   constexpr std::enable_if_t<chopsuey::is_integer<T>::value,
   cell &> operator=(T const &rhs) noexcept
      {
      this->value = gsl::narrow_cast<value_type>(rhs);
      return *this;
      }

   constexpr operator value_type const &() const noexcept
      {
      return this->value;
      }
   };

static_assert(std::is_pod<cell>::value, "");

// The tag of a cell specifies its encoding and the possible number of
// extra cells used.  Note that only the very first cell in a group of
// cells representing a Bencoded element contains a tag.  The number
// and encoding of subsequent cells (if any) is then also determined
// by this tag.
enum tag: unsigned int
   {
   list_tag   = 0b000,
   end_tag    = 0b001,
   string_tag = 0b011,
   dict_tag   = 0b100,
   int3_tag   = 0b101,
   int2_tag   = 0b111
   };

constexpr unsigned int tag_bits = 3;

constexpr cell::value_type tag_mask = (cell::value_type(1) << tag_bits) - 1;

constexpr unsigned int offset_bits =
  std::numeric_limits<cell::value_type>::digits - tag_bits;

constexpr cell::value_type max_offset =
  (cell::value_type(1) << offset_bits) - 1;

} // namespace detail
} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_CELL_HPP_INCLUDED */
