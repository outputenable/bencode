/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#ifndef CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECT_HPP_INCLUDED
#define CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECT_HPP_INCLUDED

#include <cstddef>
#include <limits>
#include <memory>
#include <vector>

#include <gsl/span>

#include "descriptor.hpp"
#include "detail/cell.hpp"
#include "item_types.hpp"

namespace contagiousbt {
namespace bencode {
namespace outline {

template <typename charT = char, typename Allocator = std::allocator<charT>>
class object
   {
 public:
   using char_type = charT;

   using allocator_type = Allocator;

   using range_type = range<basic_descriptor<char_type>>;

   object() = default;

   explicit object(allocator_type const &alloc): cells_(alloc) {}

   void clear()
      {
      cells_.clear();
      be_ = nullptr;
      }

   void shrink_to_fit() { cells_.shrink_to_fit(); }

   typename gsl::span<char_type const>::index_type
   outline
     (gsl::span<char_type const> buf,
      std::size_t const max_top_level_items,
      int &ec);

   typename gsl::span<char_type const>::index_type
   outline(gsl::span<char_type const> const buf, int &ec)
      {
      return this->outline(buf, std::numeric_limits<std::size_t>::max(), ec);
      }

   range_type root_range() const
      {
      // back() is an auxiliary cell and intentionally not part of the range
      return { &cells_.front(), &cells_.back(), be_ };
      }

   optional<descriptor> root_dict() const;

 private:
   std::vector
     <detail::cell,
      typename std::allocator_traits<allocator_type>
        ::template rebind_alloc<detail::cell>>
     cells_;

   void const *be_;
   };

} // namespace outline
} // namespace bencode
} // namespace contagiousbt

#endif /* CONTAGIOUSBT_BENCODE_OUTLINE_DETAIL_OBJECT_HPP_INCLUDED */
