# contagiousbt-bencode

on the Web: <https://gitlab.com/outputenable/bencode>

contagiousbt-bencode (or bencode for short) is a C++ header-only library for
reading and decoding data in
[Bencode](https://wiki.theory.org/index.php/BitTorrentSpecification#Bencoding "Bencode specification")
format.

[![No Maintenance Intended](http://unmaintained.tech/badge.svg)](http://unmaintained.tech/)

## License

bencode is licensed under the Boost Software License; see accompanying file
[LICENSE_1_0.txt](/LICENSE_1_0.txt) or copy at
<http://www.boost.org/LICENSE_1_0.txt>.

## Requirements

*   C++14
*   [CMake](https://cmake.org/)

<!-- -->

*   [chopsuey](https://gitlab.com/outputenable/chopsuey), header-only
*   [GSL: Guideline Support Library](https://github.com/Microsoft/GSL),
    header-only

for outline:

*   [Boost C++ Libraries](http://www.boost.org/), header-only; specifically
    *   [Boost.Container](http://www.boost.org/doc/libs/release/libs/container/),
        header-only
    *   [Boost.Iterator](http://www.boost.org/doc/libs/release/libs/iterator/doc/),
        header-only
    *   [none](http://www.boost.org/doc/libs/release/boost/none.hpp),
        header-only
    *   [Boost.Optional](http://www.boost.org/doc/libs/release/libs/optional/),
        header-only
    *   [string_view](http://www.boost.org/doc/libs/release/boost/utility/string_view.hpp),
        header-only

for unit tests (optional):

*   [Google Test](https://github.com/google/googletest)
*   [GCC](https://gcc.gnu.org/) and
    [LCOV](http://ltp.sourceforge.net/coverage/lcov.php) for code coverage

## Overview

First and foremost the bencode library provides *generic* low-level facilities
for reading and decoding data in Bencode format.
Some notable features of these are:

*   Support for stream processing

    The bencoded source does not need to be in memory as a whole, but can be
    fed to the decoder incrementally in chunks.

*   Independence of data representation

    The decoder does not produce or assume any particular in-memory data
    representation for the decoded items.
    (Possibly incremental) processing is delegated to clients via callbacks.

*   Extensibility

    Clients can extend the decoder reasonably easily and without significant
    loss of efficiency to support custom source encodings, e.g. floating
    point items.

*   No dynamic memory allocation
*   No use of [RTTI](https://en.wikipedia.org/wiki/Run-time_type_information)
*   No use of exceptions

The reader/decoder is thus intended to be used as a building block to create
client-specific Bencode parsers.
To be actually usable, it needs to be augmented with custom "back-end"
routines that perform various higher-level tasks, such as for instance
building up in-memory data structures.
The bencode library also includes the implementation of one such back-end,
providing a complete Bencode parser: the **outline**, which is described in
the next section.

## Outline parser

The basic idea of the design is described by Arvid Norberg in his blog post
about [bdecode parsers](http://blog.libtorrent.org/2015/03/bdecode-parsers/).
In short: The bencoded source is loaded into a contiguous buffer and parsed
into a sequence of tokens, each one encoding information about a single
bencoded "item": an integer, string, list, dictionary or end tag.
The tokens do not store the actual values though, but rather their location
and extent in the original bencoded source buffer (via offsets, basically).
This makes for quite a compact representation, but also means that in order to
access a value one now needs (a back link to) the original buffer in addition
to the token.
Also, accessing an integer requires another decoding step since the value is
still bencoded in that buffer.
All those details are conveniently handled by a node type which combines the
rather low-level tokens with a back link to the bencoded buffer and provides
various methods for getting at the values etc.

The outline implementation presented here is shaped by the (deliberate)
requirement that there shall be no re-scanning of the bencoded source after
the initial parse, that is all information (except for the actual string
values which do not need any decoding) shall be encoded directly into the
outline "cells" (tokens) in a binary format.
This

*   strictly decouples parsing/creation of the outline from just reading it
    and

*   further improves data cache-friendliness because accessing integer values
    doesn't need to touch/decode the source buffer (again) anymore.

Naturally, these benefits come at the expense of a more complicated encoding
scheme.
The outline uses a variable number of cells per item which allows to
maintain a low space overhead, e.g. just two 32-bit cells (in total) for a
32-bit signed integer value.
The resulting increased computational cost is mitigated by providing short
*branchless* implementations of all the common type-agnostic operations like
determining the item type, the number of cells occupied or the offset into the
bencoded source buffer.
And last but not least, the list and dictionary containers also feature a
constant-time size query.

Each Bencode item in the source buffer is represented by a number of 32-bit
*cells*.
The sequence of cells representing consecutive items forms the core data
structure of the outline.
As indicated above, the cell encoding can get quite involved and hence they're
not generally exposed in the API.
Instead, *descriptors* serve as lightweight handles for the cell(s)
representing a single Bencode item.
Sequences of descriptors, e.g. for the items in a list or dictionary
container, are in turn represented by various descriptor *ranges*.
To leverage the facilities of the standard algorithms library, these ranges
provide compatible (input) *iterators*.

```
 bencoded
 source       -->|d|2:bt|i2001e|10:contagious|l|7:bencode|i2017e|e|e|
 buffer      /    ^ ^    ^      ^             ^ ^         ^      ^ ^
             |     \ \    \____  \_           | |       _/  ____/ /
             |      \ \_____   \   \         /  |      /   / ____/
             |       \_     \   \   \       /   |     /   / /
             |         \     \   \   \     /    |    /   / /
             |         |d|1|2|s|2|i|2|s|3|l|5|2|s|2|i|2|e|e|
 cells       |         |@|5| |@| |@|0|@| |@| | |@| |@|0|@|@|
             |         |0| | |1| |5|0|1| |2| | |2| |3|1|4|4|
             |         | | | | | | |1|1| |4| | |5| |4|7|0|1|
             |                ^                 ^       ^
             ^                |                 |       |
 descriptor  |\____________|b|c|                |       |
             |             |e|s|                \___   _/
             |                                      \ /
 range       \___________________________________|b|c|c|
 (containing                                     |e|s|s|
  descriptor(s))                                   ^
                                                  /
 iterators                                      |i|   |e|
```

## Building

```shell
git clone https://gitlab.com/outputenable/bencode.git
cd bencode

# for running the unit tests, Google Test+Mock are provided as submodules and
# should be found by default
git submodule init
git submodule update

mkdir build
cd build

# even to use just the header-only facilities, chopsuey must have been built
# beforehand; it can then be used directly from its build tree (by specifying
# the appropriate CHOPSUEY_ROOT) or installed, in which case it should be
# found automatically
cmake \
  [-DBOOST_ROOT=/path/to/boost] \
  [-DCHOPSUEY_ROOT=/path/to/chopsuey/build] \
  [-DGSUPPL_ROOT=/path/to/gsl] \
  ..

cmake --build .
ctest -C debug
```
