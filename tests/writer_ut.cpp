/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <stack>
#include <streambuf>
#include <type_traits>
#include <vector>

#include <contagiousbt/bencode/outline/detail/membuf.hpp>
#include <contagiousbt/bencode/outline/detail/writer_def.hpp>

namespace outline = contagiousbt::bencode::outline;

static_assert(std::is_trivial<outline::detail::stack_frame>::value, "");

template class outline::detail::writer
  <std::vector<outline::detail::cell>,
   std::stack
     <outline::detail::stack_frame,
      std::vector<outline::detail::stack_frame>>,
   outline::detail::membuf>;
