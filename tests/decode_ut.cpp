/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <gtest/gtest.h>

#include <contagiousbt/bencode/outline/detail/decode.hpp>

namespace outline = contagiousbt::bencode::outline;

using outline::item_type;

using outline::detail::cell;

using outline::detail::int2_tag;
using outline::detail::int3_tag;
using outline::detail::string_tag;
using outline::detail::list_tag;
using outline::detail::dict_tag;
using outline::detail::end_tag;

#if defined _MSC_VER  // no ADL in static_assert()/constexpr!?
using outline::is_container;
using outline::detail::is_container;
using outline::detail::num_cells;
using outline::detail::to_item_type;
#endif

static_assert(to_item_type(int2_tag) == item_type::integer, "");
static_assert(to_item_type(int3_tag) == item_type::integer, "");
static_assert(to_item_type(string_tag) == item_type::string, "");
static_assert(to_item_type(list_tag) == item_type::list, "");
static_assert(to_item_type(dict_tag) == item_type::dict, "");
static_assert(to_item_type(end_tag) == item_type::end, "");

static_assert( !is_container(item_type::integer), "");
static_assert( !is_container(item_type::string), "");
static_assert(is_container(item_type::list), "");
static_assert(is_container(item_type::dict), "");
static_assert( !is_container(item_type::end), "");

static_assert( !is_container(cell(+int2_tag)), "");
static_assert( !is_container(cell(+int3_tag)), "");
static_assert( !is_container(cell(+string_tag)), "");
static_assert(is_container(cell(+list_tag)), "");
static_assert(is_container(cell(+dict_tag)), "");
static_assert( !is_container(cell(+end_tag)), "");

static_assert(num_cells(int2_tag) == 2, "");
static_assert(num_cells(int3_tag) == 3, "");
static_assert(num_cells(string_tag) == 2, "");
static_assert(num_cells(list_tag) == 3, "");
static_assert(num_cells(dict_tag) == 3, "");
static_assert(num_cells(end_tag) == 1, "");
