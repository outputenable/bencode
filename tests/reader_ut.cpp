/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <limits>
#include <sstream>
#include <streambuf>
#include <string>

#include <contagiousbt/bencode/reader_def.hpp>
#include <contagiousbt/bencode/status.hpp>
#include <contagiousbt/bencode/visitor_def.hpp>

namespace {

namespace bencode = contagiousbt::bencode;

using ::testing::_;
using ::testing::InSequence;
using ::testing::NiceMock;
using ::testing::Ref;
using ::testing::Return;
using ::testing::StrictMock;

template <typename Sb = std::streambuf>
class fake_visitor: public bencode::visitor<Sb>
   {
 public:
   using typename bencode::visitor<Sb>::streambuf_type;
   using typename bencode::visitor<Sb>::tag_type;
   using typename bencode::visitor<Sb>::int_type;

   fake_visitor()
      {
      ON_CALL(*this, visit_int(_, _))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      ON_CALL(*this, visit_string(_, _))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      ON_CALL(*this, visit_list_begin(_))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      ON_CALL(*this, visit_dict_begin(_))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      ON_CALL(*this, visit_end(_))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      ON_CALL(*this, visit_custom_tag(_, _))
        .WillByDefault(Return(std::numeric_limits<int>::max()));
      }

   MOCK_METHOD2_T(visit_int, int (int_type, streambuf_type &));
   MOCK_METHOD2_T(visit_string, int (int_type &, streambuf_type &));
   MOCK_METHOD1_T(visit_list_begin, int (streambuf_type &));
   MOCK_METHOD1_T(visit_dict_begin, int (streambuf_type &));
   MOCK_METHOD1_T(visit_end, int (streambuf_type &));
   MOCK_METHOD2_T(visit_custom_tag, int (tag_type, streambuf_type &));
   };

class fake_streambuf
   {
 public:
   using char_type = char;
   using traits_type = std::char_traits<char>;
   using int_type = traits_type::int_type;

   fake_streambuf()
      {
      ON_CALL(*this, sbumpc()).WillByDefault(Return(traits_type::eof()));
      }

   MOCK_METHOD0(sbumpc, int_type ());
   };

template <typename Sb = std::streambuf>
auto make_reader()
   {
   return bencode::reader<bencode::visitor<Sb>, Sb>{};
   }

class ReaderIntegerTest:
  public ::testing::TestWithParam<fake_visitor<>::int_type> {};

// Note that strictly speaking we cannot rely on the various character and
// string literals in the tests below to be encoded in ASCII at execution
// time (see C++14, 2.2 Phases of translation [lex.phases]/1(5) and
// 2.3 Character sets [lex.charset]).  If you're running this on a platform
// where they're *actually* *not* though, I'd really like to hear from you!
//   --oe, Sep 2016

template <typename T>
auto to_ascii_string(T const value) { return std::to_string(value); }

template <>
auto to_ascii_string(char const *const s) { return std::string(s); }

TEST(ReaderTest, Reader_DefaultConstruction_Succeeds)
   {
   ASSERT_NO_THROW
     ((bencode::reader<bencode::visitor<std::streambuf>, std::streambuf>{}));
   }

TEST(ReaderTest, Read_Initially_CallsSbumpc)
   {
   auto rd = make_reader<fake_streambuf>();
   fake_visitor<fake_streambuf> fake_vtor;
   fake_streambuf mock_sb;

   EXPECT_CALL(mock_sb, sbumpc())
     .WillOnce(Return(fake_streambuf::traits_type::eof()));
   (void)rd.read(fake_vtor, mock_sb);
   }

TEST(ReaderTest, Read_EmptyStreambuf_ReturnsStatusUnderflow)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf empty_sb;

   auto const rc = rd.read(fake_vtor, empty_sb);

   ASSERT_EQ(rc, bencode::status_underflow);
   }

TEST(ReaderTest, Read_EmptyInt_ReturnsStatusEmptyInt)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb(to_ascii_string("ie"));

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_empty_int);
   }

TEST(ReaderTest, Read_IntLeadingInvalidDigit_ReturnsStatusInvalidDigit)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb(to_ascii_string("iz"));

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_invalid_digit);
   }

TEST(ReaderTest, Read_IntJustMinus_ReturnsStatusMalformedInt)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb(to_ascii_string("i-e"));

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_malformed_int);
   }

TEST(ReaderTest, Read_IntMinusZero_ReturnsStatusMinusZero)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb(to_ascii_string("i-0e"));

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_minus_zero);
   }

TEST(ReaderTest, Read_IntMinusLeadingZero_ReturnsStatusLeadingZero)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb(to_ascii_string("i-01"));

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_leading_zero);
   }

TEST_P(ReaderIntegerTest, Read_Integer_VisitsIntValue)
   {
   auto rd = make_reader<>();
   fake_visitor<> mock_vtor;
   auto const value = GetParam();
   std::stringbuf sb
     (std::string{ *bencode::ascii_i }
      += to_ascii_string(value)
      += *bencode::ascii_e);

   EXPECT_CALL(mock_vtor, visit_int(value, Ref(sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, sb);
   }

INSTANTIATE_TEST_CASE_P(SingleDigits, ReaderIntegerTest,
  ::testing::Values(0, 1, 2, 3, 4, 5, 6, 7, 8, 9),);

INSTANTIATE_TEST_CASE_P(MinMax64, ReaderIntegerTest, ::testing::Values
  (INT64_C(-9'223'372'036'854'775'807) - 1,
   INT64_C(+9'223'372'036'854'775'807)),);

TEST(ReaderTest, Read_ListTag_VisitsListBegin)
   {
   auto rd = make_reader<>();
   fake_visitor<> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_ell });

   EXPECT_CALL(mock_vtor, visit_list_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_ListTag_ReturnsStatusFromVisitingListBegin)
   {
   auto rd = make_reader<>();
   NiceMock<fake_visitor<>> stub_vtor;
   ON_CALL(stub_vtor, visit_list_begin(_)).WillByDefault(Return(0xbad));
   std::stringbuf sb(std::string{ *bencode::ascii_ell });

   auto const rc = rd.read(stub_vtor, sb);

   ASSERT_EQ(rc, 0xbad);
   }

TEST(ReaderTest, Read_ListTag_RepeatedlyVisitsListBeginUntilStatusOK)
   {
   auto rd = make_reader<>();
   StrictMock<fake_visitor<>> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_ell });

   EXPECT_CALL(mock_vtor, visit_list_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, sb);
   EXPECT_CALL(mock_vtor, visit_list_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_AfterVisitingListBegin_ReadsNextTag)
   {
   auto rd = make_reader<fake_streambuf>();
   fake_visitor<fake_streambuf> mock_vtor;
   fake_streambuf mock_sb;

   InSequence inSequence;
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_ell));
   EXPECT_CALL(mock_vtor, visit_list_begin(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_ok));
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_ell));
   EXPECT_CALL(mock_vtor, visit_list_begin(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, mock_sb);
   }

TEST(ReaderTest, Read_DictTag_VisitsDictBegin)
   {
   auto rd = make_reader<>();
   fake_visitor<> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_d });

   EXPECT_CALL(mock_vtor, visit_dict_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_DictTag_ReturnsStatusFromVisitingDictBegin)
   {
   auto rd = make_reader<>();
   NiceMock<fake_visitor<>> stub_vtor;
   ON_CALL(stub_vtor, visit_dict_begin(_)).WillByDefault(Return(0xbad));
   std::stringbuf sb(std::string{ *bencode::ascii_d });

   auto const rc = rd.read(stub_vtor, sb);

   ASSERT_EQ(rc, 0xbad);
   }

TEST(ReaderTest, Read_DictTag_RepeatedlyVisitsDictBeginUntilStatusOK)
   {
   auto rd = make_reader<>();
   StrictMock<fake_visitor<>> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_d });

   EXPECT_CALL(mock_vtor, visit_dict_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, sb);
   EXPECT_CALL(mock_vtor, visit_dict_begin(Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_AfterVisitingDictBegin_ReadsNextTag)
   {
   auto rd = make_reader<fake_streambuf>();
   fake_visitor<fake_streambuf> mock_vtor;
   fake_streambuf mock_sb;

   InSequence inSequence;
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_d));
   EXPECT_CALL(mock_vtor, visit_dict_begin(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_ok));
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_d));
   EXPECT_CALL(mock_vtor, visit_dict_begin(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, mock_sb);
   }

TEST(ReaderTest, Read_EndTag_VisitsEnd)
   {
   auto rd = make_reader<>();
   fake_visitor<> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_e });

   EXPECT_CALL(mock_vtor, visit_end(_)).WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_EndTag_ReturnsStatusFromVisitingEnd)
   {
   auto rd = make_reader<>();
   NiceMock<fake_visitor<>> stub_vtor;
   ON_CALL(stub_vtor, visit_end(_)).WillByDefault(Return(0xbad));
   std::stringbuf sb(std::string{ bencode::ascii_e });

   auto const rc = rd.read(stub_vtor, sb);

   ASSERT_EQ(rc, 0xbad);
   }

TEST(ReaderTest, Read_EndTag_RepeatedlyVisitsEndUntilStatusOK)
   {
   auto rd = make_reader<>();
   StrictMock<fake_visitor<>> mock_vtor;
   std::stringbuf sb(std::string{ *bencode::ascii_e });

   EXPECT_CALL(mock_vtor, visit_end(Ref(sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, sb);
   EXPECT_CALL(mock_vtor, visit_end(Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_AfterVisitingEnd_ReadsNextTag)
   {
   auto rd = make_reader<fake_streambuf>();
   fake_visitor<fake_streambuf> mock_vtor;
   fake_streambuf mock_sb;

   InSequence inSequence;
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_e));
   EXPECT_CALL(mock_vtor, visit_end(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_ok));
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(bencode::ascii_e));
   EXPECT_CALL(mock_vtor, visit_end(Ref(mock_sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, mock_sb);
   }

TEST(ReaderTest, Read_UnknownTag_VisitsCustomTag)
   {
   auto rd = make_reader<>();
   fake_visitor<> mock_vtor;
   auto const ctag = '\25';
   std::stringbuf sb(std::string{ ctag });

   EXPECT_CALL(mock_vtor, visit_custom_tag(ctag, Ref(sb)))
     .WillOnce(Return(bencode::status_invalid_tag));
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_UnknownTag_ReturnsStatusFromVisitingCustomTag)
   {
   auto rd = make_reader<>();
   NiceMock<fake_visitor<>> stub_vtor;
   auto const status = 911;
   ON_CALL(stub_vtor, visit_custom_tag(_, _)).WillByDefault(Return(status));
   std::stringbuf sb("\25");

   auto const rc = rd.read(stub_vtor, sb);

   ASSERT_EQ(rc, status);
   }

TEST(ReaderTest, Read_UnknownTag_RepeatedlyVisitsCustomTagUntilStatusOK)
   {
   auto rd = make_reader<>();
   StrictMock<fake_visitor<>> mock_vtor;
   auto const ctag = '\25';
   std::stringbuf sb(std::string{ ctag });

   EXPECT_CALL(mock_vtor, visit_custom_tag(ctag, Ref(sb)))
     .WillOnce(Return(bencode::status_overflow));
   (void)rd.read(mock_vtor, sb);
   EXPECT_CALL(mock_vtor, visit_custom_tag(ctag, Ref(sb)))
     .WillOnce(Return(bencode::status_ok));
   (void)rd.read(mock_vtor, sb);
   (void)rd.read(mock_vtor, sb);
   }

TEST(ReaderTest, Read_AfterVisitingCustomTag_ReadsNextTag)
   {
   auto rd = make_reader<fake_streambuf>();
   fake_visitor<fake_streambuf> mock_vtor;
   fake_streambuf mock_sb;

   InSequence inSequence;
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(6));
   EXPECT_CALL(mock_vtor, visit_custom_tag('\6', Ref(mock_sb)))
     .WillOnce(Return(bencode::status_ok));
   EXPECT_CALL(mock_sb, sbumpc()).WillOnce(Return(7));
   EXPECT_CALL(mock_vtor, visit_custom_tag('\7', Ref(mock_sb)))
     .WillOnce(Return(bencode::status_invalid_tag));
   (void)rd.read(mock_vtor, mock_sb);
   }

TEST(ReaderTest, Read_ColonAsTag_ReturnsStatusEmptyStringLength)
   {
   auto rd = make_reader<>();
   fake_visitor<> fake_vtor;
   std::stringbuf sb({ *bencode::ascii_colon });

   auto const rc = rd.read(fake_vtor, sb);

   ASSERT_EQ(rc, bencode::status_empty_string_length);
   }

} // unnamed namespace
