/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <contagiousbt/bencode/outline/object_def.hpp>

namespace outline = contagiousbt::bencode::outline;

template class outline::object<>;
