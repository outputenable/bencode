/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2016.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <gtest/gtest.h>

#include <array>
#include <limits>
#include <type_traits>

#include <contagiousbt/bencode/outline/descriptor.hpp>
#include <contagiousbt/bencode/outline/detail/cell.hpp>

namespace outline = contagiousbt::bencode::outline;

using outline::item_type;

using outline::detail::cell;

using outline::detail::int2_tag;
using outline::detail::int3_tag;
using outline::detail::string_tag;
using outline::detail::list_tag;
using outline::detail::dict_tag;
using outline::detail::end_tag;

template class outline::range_base
  <outline::stride::physical, outline::descriptor>;
template class outline::range_base
  <outline::stride::logical, outline::descriptor>;

namespace {

class test_descriptor: public outline::descriptor_base
   {
 public:
   explicit test_descriptor
     (outline::detail::cell const *const c,
      void const *const be = nullptr):
     outline::descriptor_base(c, be)
      {}

   test_descriptor() = default;

   using outline::descriptor_base::num_cells;
   using outline::descriptor_base::cells_total;
   };

class DescriptorTest: public ::testing::Test
   {
 public:
   using uint_type = std::make_unsigned_t<test_descriptor::int_type>;

   DescriptorTest(): cells{}, desc(&this->cells[0], this->be.data()) {}

   std::array<cell, 4> cells;
   std::array<char, 8> be;
   test_descriptor desc;
   };

TEST_F(DescriptorTest, OperatorBool_DefaultConstructed_ReturnsFalse)
   {
   auto const td = test_descriptor();

   ASSERT_FALSE(bool(td));
   }

TEST_F(DescriptorTest, OperatorBool_HasCell_ReturnsTrue)
   {
   auto const td = test_descriptor(&this->cells[0], this->be.data());

   ASSERT_TRUE(bool(td));
   }

TEST_F(DescriptorTest, Type_SmallIntCell_ReturnsInteger)
   {
   this->cells[0] = +int2_tag;

   auto const type = this->desc.type();

   ASSERT_EQ(type, item_type::integer);
   }

TEST_F(DescriptorTest, IsInt_SmallIntCell_ReturnsTrue)
   {
   this->cells[0] = +int2_tag;

   auto const ii = this->desc.is_int();

   ASSERT_TRUE(ii);
   }

TEST_F(DescriptorTest, Type_BigIntCell_ReturnsInteger)
   {
   this->cells[0] = +int3_tag;

   auto const type = this->desc.type();

   ASSERT_EQ(type, item_type::integer);
   }

TEST_F(DescriptorTest, IsInt_BigIntCell_ReturnsTrue)
   {
   this->cells[0] = +int3_tag;

   auto const ii = this->desc.is_int();

   ASSERT_TRUE(ii);
   }

class DescriptorSmallIntegersTest:
  public DescriptorTest,
  public ::testing::WithParamInterface<test_descriptor::int_type>
   {
 public:
   void SetUp() override
      {
      auto const s32 =
        static_cast<std::make_signed_t<cell::value_type>>(GetParam());
      auto const &u32 = reinterpret_cast<cell::value_type const &>(s32);
      this->cells[0] = +int2_tag;
      this->cells[1] = u32;
      }
   };

class DescriptorBigIntegersTest:
  public DescriptorTest,
  public ::testing::WithParamInterface<test_descriptor::int_type>
   {
 public:
   void SetUp() override
      {
      auto const &u64 = reinterpret_cast<uint_type const &>(GetParam());
      this->cells[0] = +int3_tag;
      this->cells[1] = u64 >> 32;
      this->cells[2] = u64 & UINT32_MAX;
      }
   };

TEST_P(DescriptorSmallIntegersTest, NumCells_SmallInteger_ReturnsTwo)
   {
   auto const ncells = this->desc.num_cells();

   ASSERT_EQ(ncells, 2);
   }

TEST_P(DescriptorSmallIntegersTest, CellsTotal_SmallInteger_ReturnsTwo)
   {
   auto const cells_total = this->desc.cells_total();

   ASSERT_EQ(cells_total, 2);
   }

TEST_P(DescriptorSmallIntegersTest, GetInt_SmallInteger_ReturnsValue)
   {
   auto const i = this->desc.get_int().value();

   ASSERT_EQ(i, GetParam());
   }

INSTANTIATE_TEST_CASE_P(SmallValues, DescriptorSmallIntegersTest, ::testing::Values
  (INT32_MIN, -1, 0, 1, INT32_MAX),);

TEST_P(DescriptorBigIntegersTest, NumCells_BigInteger_ReturnsThree)
   {
   auto const ncells = this->desc.num_cells();

   ASSERT_EQ(ncells, 3);
   }

TEST_P(DescriptorBigIntegersTest, CellsTotal_BigInteger_ReturnsThree)
   {
   auto const cells_total = this->desc.cells_total();

   ASSERT_EQ(cells_total, 3);
   }

TEST_P(DescriptorBigIntegersTest, GetInt_BigInteger_ReturnsValue)
   {
   auto const i = this->desc.get_int().value();

   ASSERT_EQ(i, GetParam());
   }

INSTANTIATE_TEST_CASE_P(LargeValues, DescriptorBigIntegersTest, ::testing::Values
  (std::numeric_limits<test_descriptor::int_type>::min(),
   test_descriptor::int_type(INT32_MIN) - 1,
   test_descriptor::int_type(INT32_MAX) + 1,
   std::numeric_limits<test_descriptor::int_type>::max()),);

TEST_F(DescriptorTest, Type_String_ReturnsString)
   {
   this->cells[0] = +string_tag;

   auto const type = this->desc.type();

   ASSERT_EQ(type, item_type::string);
   }

TEST_F(DescriptorTest, IsString_String_ReturnsTrue)
   {
   this->cells[0] = +string_tag;

   auto const is = this->desc.is_string();

   ASSERT_TRUE(is);
   }

TEST_F(DescriptorTest, NumCells_String_ReturnsTwo)
   {
   this->cells[0] = +string_tag;

   auto const ncells = this->desc.num_cells();

   ASSERT_EQ(ncells, 2);
   }

TEST_F(DescriptorTest, CellsTotal_String_ReturnsTwo)
   {
   this->cells[0] = +string_tag;

   auto const cells_total = this->desc.cells_total();

   ASSERT_EQ(cells_total, 2);
   }

TEST_F(DescriptorTest, GetString_String_ReturnsString)
   {
   constexpr auto offset = 1U;
   constexpr auto disp = 2U;
   constexpr auto len = 3U;
   this->cells[0] = (offset << 3) | string_tag;
   this->cells[1] = disp;
   this->cells[2] = (offset + disp + len) << 3;

   auto const str = this->desc.get_string().value();

   auto const data_offs = static_cast<std::ptrdiff_t>(offset + disp);
   ASSERT_EQ(str.data(), &this->be[data_offs]);
   ASSERT_EQ(str.length(), len);
   }

} // unnamed namespace
