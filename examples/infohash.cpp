/*
   This file is part of contagiousbt/bencode.

   Copyright Oliver Ebert 2017.

   Distributed under the Boost Software License, Version 1.0.
      (See accompanying file LICENSE_1_0.txt or copy at
            http://www.boost.org/LICENSE_1_0.txt)
*/

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <iomanip>
#include <ios>
#include <iostream>
#include <limits>
#include <memory>
#include <sstream>
#include <string>

#include <gsl/span>

#include <chopsuey/sha1.hpp>
#include <chopsuey/utility.hpp>

#include <contagiousbt/bencode/outline/descriptor.hpp>
#include <contagiousbt/bencode/outline/object_def.hpp>
#include <contagiousbt/bencode/status.hpp>

namespace bencode = contagiousbt::bencode;
namespace outline = bencode::outline;

int
main(int argc, char **argv)
   {
   if (argc != 2)
      {
      std::cerr << "usage: "
        << (argv[0] ? argv[0] : "infohash") << " <.torrent file>\n";
      return EXIT_FAILURE;
      }

   std::string const filename(argv[1]);

   // read file contents into a contiguous memory buffer
   std::unique_ptr<char[]> buf_ptr;
   gsl::span<char const> buf;
   {
      std::ifstream is(filename, std::ios_base::in | std::ios_base::binary);
      is.exceptions
        (  std::ios_base::badbit
         | std::ios_base::failbit
         | std::ios_base::eofbit);

      auto const org_pos = is.tellg();
      is.seekg(0, std::ios_base::end);
      auto const file_size =
        static_cast<std::streamoff>(is.tellg() - org_pos);
      is.seekg(org_pos);

      if (file_size > std::numeric_limits<std::ptrdiff_t>::max())
         {
         std::cerr << "The file is too large!\n";
         return EXIT_FAILURE;
         }

      buf_ptr.reset(new char[static_cast<std::size_t>(file_size)]);
      is.read(buf_ptr.get(), static_cast<std::streamsize>(file_size));

      buf = { buf_ptr.get(), static_cast<std::ptrdiff_t>(file_size) };
      }

   // create an outline of the (presumably) bencoded source data in buf, then
   // find the info dictionary and determine its extent in the bencoded source
   gsl::span<char const> be_info;
   {
      outline::object<> obj;
      int ec{};
      auto const offset = obj.outline(buf, 1, ec);
      if ( !(ec == bencode::status_ok || ec == bencode::status_done))
         {
         std::cerr << "Bencode parsing failed near offset " << offset << ": "
           << bencode::strstatus(ec) << " [" << ec << "]!\n";
         return EXIT_FAILURE;
         }

      outline::descriptor metainfo;
      if (auto const od = obj.root_dict())
         metainfo = *od;
      else
         {
         std::cerr << "The top-level item is not a dictionary!\n";
         return EXIT_FAILURE;
         }

      auto mi_rng = metainfo.to_kv_range();
      auto const ii =
#if 1
        mi_rng.find("info") ? mi_rng.cbegin() : mi_rng.cend();
          // note that we're only converting to an iterator in order to become
          // compatible with the common code below; otherwise we'd just use
          // the range directly
#else
        std::find_if
          (mi_rng.cbegin(), mi_rng.cend(),
           [](auto const &value)
              {
              auto const &k = value.first;
              return k.is_string() && k.to_string() == "info";
              });
#endif
      if (ii == mi_rng.cend())
         {
         std::cerr
           << "The top-level dictionary does not contain an 'info' key!\n";
         return EXIT_FAILURE;
         }

      if ( !ii->second.is_dict())
         {
         std::cerr << "The 'info' key does not map to a dictionary!\n";
         return EXIT_FAILURE;
         }

      be_info = ii->second.be_view();
        // be_info is a view into the original bencoded source buffer (buf)
        // and does not depend on any of the outline objects, which at this
        // point can safely be destructed
      }

   // compute SHA-1 hash value (or message digest) of the info dictionary
   unsigned char md[chopsuey::sha1::hash_size];
   {
      chopsuey::sha1 sha1;
      sha1.update(be_info);
      (void)sha1.complete(md);
      }

   // print the hash value as a hexadecimal number
   std::ostringstream infohash;
   infohash.setf(std::ios_base::hex, std::ios_base::basefield);
   infohash.fill('0');
   for (auto const b : md)
      infohash << std::setw(2) << +b;

   std::cout << infohash.str() << '\n';

   return EXIT_SUCCESS;
   }
